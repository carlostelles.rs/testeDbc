package xyz.carlostelles.testeDbc.persistence;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cliente")
public class ClienteEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    @Column(name = "nome")
    private String nome;
    @Column(name = "limite_credito")
    private Double limiteCredito;
    @Column(name = "risco")
    private String risco;
    @Column(name = "taxa_juros")
    private Double taxaJuros;

    public ClienteEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getLimiteCredito() {
        return limiteCredito;
    }

    public void setLimiteCredito(Double limiteCredito) {
        this.limiteCredito = limiteCredito;
    }

    public String getRisco() {
        return risco;
    }

    public void setRisco(String risco) {
        this.risco = risco;
    }

    public Double getTaxaJuros() {
        return taxaJuros;
    }

    public void setTaxaJuros(Double taxaJuros) {
        this.taxaJuros = taxaJuros;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClienteEntity entity = (ClienteEntity) o;
        return Objects.equals(id, entity.id)
                && Objects.equals(nome, entity.nome)
                && Objects.equals(limiteCredito, entity.limiteCredito)
                && Objects.equals(taxaJuros, entity.taxaJuros)
                && Objects.equals(risco, entity.risco);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nome, limiteCredito, taxaJuros, risco);
    }

    @Override
    public String toString() {
        return "ClienteEntity{"
                + "id=" + id
                + ", nome='" + nome + "'"
                + ", limiteCredito=" + limiteCredito
                + ", taxaJuros=" + taxaJuros
                + ", risco='" + risco + "'"
                + '}';
    }
}
