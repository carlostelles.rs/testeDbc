package xyz.carlostelles.testeDbc.persistence;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "endereco")
public class EnderecoEntity {

    @Id
    @Column(name = "cliente_id")
    private Long id;
    @Column(name = "logradouro")
    private String logradouro;
    @Column(name = "numero")
    private String numero;
    @Column(name = "complemento")
    private String complemento;
    @Column(name = "bairro")
    private String bairro;
    @Column(name = "cep")
    private String cep;
    @Column(name = "cidade")
    private String cidade;
    @Column(name = "uf")
    private String uf;

    public EnderecoEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EnderecoEntity entity = (EnderecoEntity) o;
        return Objects.equals(id, entity.id)
                && Objects.equals(logradouro, entity.logradouro)
                && Objects.equals(numero, entity.numero)
                && Objects.equals(complemento, entity.complemento)
                && Objects.equals(bairro, entity.bairro)
                && Objects.equals(cep, entity.cep)
                && Objects.equals(cidade, entity.cidade)
                && Objects.equals(uf, entity.uf);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, logradouro, numero, complemento, bairro, cep, cidade, uf);
    }

    @Override
    public String toString() {
        return "ClienteEntity{"
                + "id=" + id
                + ", logradouro='" + logradouro + "'"
                + ", numero='" + numero + "'"
                + ", complemento='" + complemento + "'"
                + ", bairro='" + bairro + "'"
                + ", cep='" + cep + "'"
                + ", cidade='" + cidade + "'"
                + ", uf='" + uf + "'"
                + '}';
    }
}
