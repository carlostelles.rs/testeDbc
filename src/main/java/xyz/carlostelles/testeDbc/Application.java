package xyz.carlostelles.testeDbc;

import org.springframework.boot.SpringApplication;
import xyz.carlostelles.testeDbc.config.Configuration;

public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Configuration.class, args);
    }
}
