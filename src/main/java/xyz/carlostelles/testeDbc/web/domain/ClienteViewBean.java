package xyz.carlostelles.testeDbc.web.domain;

import java.util.Objects;

public class ClienteViewBean {

    private Long id;
    private String name;
    private Double limiteCredito;
    private String risco;
    private Double taxaJuros;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLimiteCredito() {
        return limiteCredito;
    }

    public void setLimiteCredito(Double limiteCredito) {
        this.limiteCredito = limiteCredito;
    }

    public String getRisco() {
        return risco;
    }

    public void setRisco(String risco) {
        this.risco = risco;
    }

    public Double getTaxaJuros() {
        return taxaJuros;
    }

    public void setTaxaJuros(Double taxaJuros) {
        this.taxaJuros = taxaJuros;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClienteViewBean viewBean = (ClienteViewBean) o;
        return Objects.equals(id, viewBean.id)
                && Objects.equals(name, viewBean.name)
                && Objects.equals(limiteCredito, viewBean.limiteCredito)
                && Objects.equals(taxaJuros, viewBean.taxaJuros)
                && Objects.equals(risco, viewBean.risco);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, limiteCredito, taxaJuros, risco);
    }

    @Override
    public String toString() {
        return "ClienteViewBean{"
                + "id=" + id
                + ", name='" + name + "'"
                + ", name=" + taxaJuros
                + ", name=" + limiteCredito
                + ", name='" + risco + "'"
                + '}';
    }
}
