package xyz.carlostelles.testeDbc.web.domain;

import javax.persistence.Column;
import java.util.Objects;

public class EnderecoViewBean {

    private Long id;
    private String logradouro;
    private String numero;
    private String complemento;
    private String bairro;
    private String cep;
    private String cidade;
    private String uf;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EnderecoViewBean viewBean = (EnderecoViewBean) o;
        return Objects.equals(id, viewBean.id)
                && Objects.equals(logradouro, viewBean.logradouro)
                && Objects.equals(numero, viewBean.numero)
                && Objects.equals(complemento, viewBean.complemento)
                && Objects.equals(bairro, viewBean.bairro)
                && Objects.equals(cep, viewBean.cep)
                && Objects.equals(cidade, viewBean.cidade)
                && Objects.equals(uf, viewBean.uf);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, logradouro, numero, complemento, bairro, cep, cidade, uf);
    }

    @Override
    public String toString() {
        return "ClienteViewBean{"
                + "id=" + id
                + ", logradouro='" + logradouro + "'"
                + ", numero='" + numero + "'"
                + ", complemento='" + complemento + "'"
                + ", bairro='" + bairro + "'"
                + ", cep='" + cep + "'"
                + ", cidade='" + cidade + "'"
                + ", uf='" + uf + "'"
                + '}';
    }
}
