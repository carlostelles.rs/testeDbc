package xyz.carlostelles.testeDbc.web.converter;

import org.springframework.stereotype.Component;
import xyz.carlostelles.testeDbc.persistence.EnderecoEntity;
import xyz.carlostelles.testeDbc.web.domain.EnderecoViewBean;

@Component
public class EnderecoConverter extends Converter<EnderecoEntity, EnderecoViewBean> {

    public EnderecoViewBean encode(EnderecoEntity entity) {
        EnderecoViewBean viewBean = new EnderecoViewBean();
        viewBean.setId(entity.getId());
        viewBean.setLogradouro(entity.getLogradouro());
        viewBean.setNumero(entity.getNumero());
        viewBean.setComplemento(entity.getComplemento());
        viewBean.setBairro(entity.getBairro());
        viewBean.setCep(entity.getCep());
        viewBean.setCidade(entity.getCidade());
        viewBean.setUf(entity.getUf());

        return viewBean;
    }

    public EnderecoEntity decode(EnderecoViewBean viewBean) {
        EnderecoEntity entity = new EnderecoEntity();
        entity.setId(viewBean.getId());
        entity.setLogradouro(viewBean.getLogradouro());
        entity.setNumero(viewBean.getNumero());
        entity.setComplemento(viewBean.getComplemento());
        entity.setBairro(viewBean.getBairro());
        entity.setCep(viewBean.getCep());
        entity.setCidade(viewBean.getCidade());
        entity.setUf(viewBean.getUf());

        return entity;
    }
}
