package xyz.carlostelles.testeDbc.web.converter;

import org.springframework.stereotype.Component;
import xyz.carlostelles.testeDbc.persistence.ClienteEntity;
import xyz.carlostelles.testeDbc.web.domain.ClienteViewBean;

@Component
public class ClienteConverter extends Converter<ClienteEntity, ClienteViewBean> {

    public ClienteViewBean encode(ClienteEntity entity) {
        ClienteViewBean viewBean = new ClienteViewBean();
        viewBean.setId(entity.getId());
        viewBean.setName(entity.getNome());
        viewBean.setLimiteCredito(entity.getLimiteCredito());
        viewBean.setRisco(entity.getRisco());
        viewBean.setTaxaJuros(entity.getTaxaJuros());

        return viewBean;
    }

    public ClienteEntity decode(ClienteViewBean viewBean) {
        ClienteEntity entity = new ClienteEntity();
        entity.setId(viewBean.getId());
        entity.setNome(viewBean.getName());
        entity.setLimiteCredito(viewBean.getLimiteCredito());
        entity.setRisco(viewBean.getRisco());

        switch (viewBean.getRisco()) {
            case "A":
                entity.setTaxaJuros(0.);
                break;
            case "B":
                entity.setTaxaJuros(10.);
                break;
            case "C":
                entity.setTaxaJuros(20.);
                break;
        }

        return entity;
    }
}
