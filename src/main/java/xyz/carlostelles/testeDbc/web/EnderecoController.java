package xyz.carlostelles.testeDbc.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xyz.carlostelles.testeDbc.business.service.EnderecoService;
import xyz.carlostelles.testeDbc.persistence.EnderecoEntity;
import xyz.carlostelles.testeDbc.web.converter.EnderecoConverter;
import xyz.carlostelles.testeDbc.web.domain.EnderecoViewBean;

import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/endereco")
public class EnderecoController {

    private final EnderecoService service;
    private final EnderecoConverter converter;

    @Autowired
    public EnderecoController(EnderecoService service, EnderecoConverter converter) {
        this.service = service;
        this.converter = converter;
    }

    @GetMapping
    public List<EnderecoViewBean> get() {
        return converter.encodeAll(service.get());
    }

    @GetMapping(value = "{id}")
    public EnderecoViewBean get(@PathVariable Long id) {
        return converter.encode(service.get(id));
    }

    @PostMapping
    public EnderecoViewBean create(@RequestBody EnderecoViewBean viewBean) {
        EnderecoEntity saved = service.create(converter.decode(viewBean));

        return converter.encode(saved);
    }

    @PutMapping(value = "{id}")
    public void update(@PathVariable Long id, @RequestBody EnderecoViewBean viewBean) {
        service.update(converter.decode(viewBean));
    }

    @DeleteMapping(value = "{id}")
    public void delete(@PathVariable Long id) {
        service.delete(service.get(id));
    }
}
