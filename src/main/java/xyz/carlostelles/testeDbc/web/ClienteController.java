package xyz.carlostelles.testeDbc.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xyz.carlostelles.testeDbc.business.service.ClienteService;
import xyz.carlostelles.testeDbc.persistence.ClienteEntity;
import xyz.carlostelles.testeDbc.web.converter.ClienteConverter;
import xyz.carlostelles.testeDbc.web.domain.ClienteViewBean;

import java.util.List;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/cliente")
public class ClienteController {

    private final ClienteService service;
    private final ClienteConverter converter;

    @Autowired
    public ClienteController(ClienteService service, ClienteConverter converter) {
        this.service = service;
        this.converter = converter;
    }

    @GetMapping
    public List<ClienteViewBean> get() {
        return converter.encodeAll(service.get());
    }

    @GetMapping(value = "{id}")
    public ClienteViewBean get(@PathVariable Long id) {
        return converter.encode(service.get(id));
    }

    @PostMapping
    public ClienteViewBean create(@RequestBody ClienteViewBean viewBean) {
        ClienteEntity saved = service.create(converter.decode(viewBean));

        return converter.encode(saved);
    }

    @PutMapping(value = "{id}")
    public void update(@PathVariable Long id, @RequestBody ClienteViewBean viewBean) {
        service.update(converter.decode(viewBean));
    }

    @DeleteMapping(value = "{id}")
    public void delete(@PathVariable Long id) {
        service.delete(service.get(id));
    }
}
