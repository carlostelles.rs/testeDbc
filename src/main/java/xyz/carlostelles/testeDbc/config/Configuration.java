package xyz.carlostelles.testeDbc.config;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import xyz.carlostelles.testeDbc.Application;

@SpringBootApplication(scanBasePackageClasses = Application.class)
@EnableJpaRepositories(basePackageClasses = Application.class)
@EntityScan(basePackageClasses = Application.class)
public class Configuration {
}
