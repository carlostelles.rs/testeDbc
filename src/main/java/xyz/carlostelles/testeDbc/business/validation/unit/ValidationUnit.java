package xyz.carlostelles.testeDbc.business.validation.unit;

import java.util.List;

public interface ValidationUnit<T> {

    List<String> validateCreate(T entity);

    List<String> validateUpdate(T entity);
}
