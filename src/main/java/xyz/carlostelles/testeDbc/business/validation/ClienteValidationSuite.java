package xyz.carlostelles.testeDbc.business.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.carlostelles.testeDbc.business.validation.unit.ClienteValidator;
import xyz.carlostelles.testeDbc.persistence.ClienteEntity;

@Component
public class ClienteValidationSuite extends ValidationGenericSuite<ClienteEntity, ClienteValidator>
        implements ValidationSuite<ClienteEntity> {

    @Autowired
    public ClienteValidationSuite(ClienteValidator validator) {
        super(validator);
    }
}
