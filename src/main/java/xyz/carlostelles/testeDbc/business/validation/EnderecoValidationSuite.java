package xyz.carlostelles.testeDbc.business.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import xyz.carlostelles.testeDbc.business.validation.unit.EnderecoValidator;
import xyz.carlostelles.testeDbc.persistence.EnderecoEntity;

@Component
public class EnderecoValidationSuite extends ValidationGenericSuite<EnderecoEntity, EnderecoValidator>
        implements ValidationSuite<EnderecoEntity> {

    @Autowired
    public EnderecoValidationSuite(EnderecoValidator validator) {
        super(validator);
    }
}
