package xyz.carlostelles.testeDbc.business.validation.unit;

import org.springframework.stereotype.Component;
import xyz.carlostelles.testeDbc.persistence.EnderecoEntity;
import xyz.carlostelles.testeDbc.persistence.EnderecoRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class EnderecoValidator implements ValidationUnit<EnderecoEntity> {

    private static final int TEXT_MAX_SIZE = 20;
    private static final String VIOLATION_CREATE_ID_MESSAGE = "The ID must be null";
    private static final String VIOLATION_UPDATE_ID_MESSAGE = "The ID can not be null and must be from an existing register";
    private static final String VIOLATION_TEXT_MESSAGE = "The %s of can't be null, empty or greater than %s characters";
    private final EnderecoRepository repository;

    public EnderecoValidator(EnderecoRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<String> validateCreate(EnderecoEntity entity) {
        List<String> message = new ArrayList<>();

        validateIdCreate(entity, message);
        validateLogradouro(entity, message);

        return message;
    }

    @Override
    public List<String> validateUpdate(EnderecoEntity entity) {
        List<String> message = new ArrayList<>();

        validateIdUpdate(entity, message);
        validateLogradouro(entity, message);

        return message;
    }

    private void validateIdUpdate(EnderecoEntity entity, List<String> message) {
        if (Objects.isNull(entity.getId()) || !repository.exists(entity.getId())) {
            message.add(VIOLATION_UPDATE_ID_MESSAGE);
        }
    }

    private void validateIdCreate(EnderecoEntity entity, List<String> message) {
        if (Objects.nonNull(entity.getId())) {
            message.add(VIOLATION_CREATE_ID_MESSAGE);
        }
    }

    private void validateLogradouro(EnderecoEntity entity, List<String> message) {
        if (Objects.isNull(entity.getLogradouro()) || entity.getLogradouro().isEmpty() || isGreaterThanMaxSize(entity)) {
            message.add(String.format(VIOLATION_TEXT_MESSAGE, "Logradouro", TEXT_MAX_SIZE));
        }
    }

    private boolean isGreaterThanMaxSize(EnderecoEntity entity) {
        return entity.getLogradouro().length() > TEXT_MAX_SIZE;
    }
}
