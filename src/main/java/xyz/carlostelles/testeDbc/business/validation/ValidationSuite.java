package xyz.carlostelles.testeDbc.business.validation;

public interface ValidationSuite<E> {
    void validateToCreate(E entity);
    void validateToUpdate(E entity);
}
