package xyz.carlostelles.testeDbc.business.validation.unit;

import org.springframework.stereotype.Component;
import xyz.carlostelles.testeDbc.persistence.ClienteEntity;
import xyz.carlostelles.testeDbc.persistence.ClienteRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Component
public class ClienteValidator implements ValidationUnit<ClienteEntity> {

    private static final int TEXT_MAX_SIZE = 20;
    private static final String VIOLATION_CREATE_ID_MESSAGE = "The ID must be null";
    private static final String VIOLATION_UPDATE_ID_MESSAGE = "The ID can not be null and must be from an existing register";
    private static final String VIOLATION_TEXT_MESSAGE = "The %s of can't be null, empty or greater than %s characters";
    private final ClienteRepository repository;

    public ClienteValidator(ClienteRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<String> validateCreate(ClienteEntity entity) {
        List<String> message = new ArrayList<>();

        validateIdCreate(entity, message);
        validateName(entity, message);

        return message;
    }

    @Override
    public List<String> validateUpdate(ClienteEntity entity) {
        List<String> message = new ArrayList<>();

        validateIdUpdate(entity, message);
        validateName(entity, message);

        return message;
    }

    private void validateIdUpdate(ClienteEntity entity, List<String> message) {
        if (Objects.isNull(entity.getId()) || !repository.exists(entity.getId())) {
            message.add(VIOLATION_UPDATE_ID_MESSAGE);
        }
    }

    private void validateIdCreate(ClienteEntity entity, List<String> message) {
        if (Objects.nonNull(entity.getId())) {
            message.add(VIOLATION_CREATE_ID_MESSAGE);
        }
    }

    private void validateName(ClienteEntity entity, List<String> message) {
        if (Objects.isNull(entity.getNome()) || entity.getNome().isEmpty() || isGreaterThanMaxSize(entity)) {
            message.add(String.format(VIOLATION_TEXT_MESSAGE, "Nome", TEXT_MAX_SIZE));
        }
    }

    private boolean isGreaterThanMaxSize(ClienteEntity entity) {
        return entity.getNome().length() > TEXT_MAX_SIZE;
    }
}
