package xyz.carlostelles.testeDbc.business.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.carlostelles.testeDbc.business.validation.ClienteValidationSuite;
import xyz.carlostelles.testeDbc.persistence.ClienteEntity;
import xyz.carlostelles.testeDbc.persistence.ClienteRepository;

@Service
public class ClienteService extends GenericService<ClienteEntity, ClienteRepository, ClienteValidationSuite> {

    private final ClienteRepository repository;
    private final ClienteValidationSuite validator;

    @Autowired
    public ClienteService(ClienteRepository repository, ClienteValidationSuite validator) {
        super(repository, validator);
        this.repository = repository;
        this.validator = validator;
    }

}
