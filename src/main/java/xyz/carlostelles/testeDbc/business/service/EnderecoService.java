package xyz.carlostelles.testeDbc.business.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.carlostelles.testeDbc.business.validation.EnderecoValidationSuite;
import xyz.carlostelles.testeDbc.persistence.EnderecoEntity;
import xyz.carlostelles.testeDbc.persistence.EnderecoRepository;

@Service
public class EnderecoService extends GenericService<EnderecoEntity, EnderecoRepository, EnderecoValidationSuite> {

    private final EnderecoRepository repository;
    private final EnderecoValidationSuite validator;

    @Autowired
    public EnderecoService(EnderecoRepository repository, EnderecoValidationSuite validator) {
        super(repository, validator);
        this.repository = repository;
        this.validator = validator;
    }

}
