package xyz.carlostelles.testeDbc.business.business.validation.unit.cliente;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import xyz.carlostelles.testeDbc.business.validation.unit.ClienteValidator;
import xyz.carlostelles.testeDbc.persistence.ClienteEntity;
import xyz.carlostelles.testeDbc.persistence.ClienteRepository;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ClienteUpdateValidatorTest {

    private static final Long EXISTANT_ID = 1L;
    private static final Long UNEXISTANT_ID = 2L;

    @Mock
    private ClienteRepository repository;

    @InjectMocks
    private ClienteValidator validator;

    @Test
    public void shouldPassValidation() {
        // given
        ClienteEntity entity = new ClienteEntity();
        entity.setId(EXISTANT_ID);

        Mockito.when(repository.exists(EXISTANT_ID)).thenReturn(true);

        // when
        List<String> violation = validator.validateUpdate(entity);

        // then
        Assert.assertTrue(violation.isEmpty());
    }

    @Test
    public void shouldFailWhenIdIsNull() {
        // given
        ClienteEntity entity = new ClienteEntity();
        entity.setId(null);

        Mockito.when(repository.exists(Matchers.anyLong())).thenReturn(true);

        // when
        List<String> violation = validator.validateUpdate(entity);

        // then
        Assert.assertFalse(violation.isEmpty());
    }

    @Test
    public void shouldFailWhenIdNotExists() {
        // given
        ClienteEntity entity = new ClienteEntity();
        entity.setId(UNEXISTANT_ID);

        Mockito.when(repository.exists(Matchers.anyLong())).thenReturn(true);
        Mockito.when(repository.exists(UNEXISTANT_ID)).thenReturn(false);

        // when
        List<String> violation = validator.validateUpdate(entity);

        // then
        Assert.assertFalse(violation.isEmpty());
    }
}