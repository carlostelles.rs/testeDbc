package xyz.carlostelles.testeDbc.business.business.validation;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import xyz.carlostelles.testeDbc.business.validation.ValidationException;
import xyz.carlostelles.testeDbc.business.validation.ValidationSuiteExecutor;
import xyz.carlostelles.testeDbc.business.validation.unit.ValidationUnit;
import xyz.carlostelles.testeDbc.persistence.ClienteEntity;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class ValidationSuiteExecutorTest {

    @InjectMocks
    private ValidationSuiteExecutor executor;

    @Test(expected = ValidationException.class)
    public void shouldThrowsExceptionWhenUnitFail() {
        // given
        ValidationUnit unit = Mockito.mock(ValidationUnit.class);
        List<ValidationUnit<ClienteEntity>> units = Collections.singletonList(unit);
        ClienteEntity entity = new ClienteEntity();
        String violationMessage = "Any message";

        Mockito.when(unit.validateCreate(entity)).thenReturn(Collections.singletonList("Other message"));

        // when
        executor.runUnitsCreate(units, entity);

        // then
        // throws exception
    }

    @Test
    public void shouldNotThrowsExceptionWhenUnitPass() {
        // given
        ValidationUnit unit = Mockito.mock(ValidationUnit.class);
        List<ValidationUnit<ClienteEntity>> units = Collections.singletonList(unit);
        ClienteEntity entity = new ClienteEntity();
        String violationMessage = "Any message";

        Mockito.when(unit.validateCreate(entity)).thenReturn(Collections.singletonList(Optional.empty()));

        // when
        executor.runUnitsCreate(units, entity);

        // then
        // throws exception
    }

}