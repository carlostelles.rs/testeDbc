package xyz.carlostelles.testeDbc.business.business.validation.unit.cliente;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import xyz.carlostelles.testeDbc.business.validation.unit.ClienteValidator;
import xyz.carlostelles.testeDbc.persistence.ClienteEntity;
import xyz.carlostelles.testeDbc.persistence.ClienteRepository;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ClienteBadUpdateValidatorTest {

    private static final Long EXISTANT_ID = 1L;
    private static final Long UNEXISTANT_ID = 2L;
    private static final String VALID_NAME = "Pudding 2";
    private static final String VALID_ICON = "Pudding 2";

    @Mock
    private ClienteRepository repository;

    @InjectMocks
    private ClienteValidator validator;


    @Test
    public void shouldPassValidation() {
        // given
        ClienteEntity entity = new ClienteEntity();
        entity.setId(EXISTANT_ID);
        entity.setNome(VALID_NAME);

        Mockito.when(repository.exists(EXISTANT_ID)).thenReturn(true);

        // when
        List<String> violation = validator.validateUpdate(entity);

        // then
        Assert.assertTrue(violation.isEmpty());
    }

    @Test
    public void shouldFailWhenIdNotExists() {
        // given
        ClienteEntity entity = new ClienteEntity();
        entity.setId(UNEXISTANT_ID);

        // will be .thenReturn(false) to any case
        Mockito.when(repository.exists(UNEXISTANT_ID)).thenReturn(false);

        // when
        List<String> violation = validator.validateUpdate(entity);

        // then
        Assert.assertTrue(!violation.isEmpty());
    }


}