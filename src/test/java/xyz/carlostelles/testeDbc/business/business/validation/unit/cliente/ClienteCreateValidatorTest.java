package xyz.carlostelles.testeDbc.business.business.validation.unit.cliente;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.InjectMocks;
import xyz.carlostelles.testeDbc.business.validation.unit.ClienteValidator;
import xyz.carlostelles.testeDbc.persistence.ClienteEntity;

import java.util.List;

public class ClienteCreateValidatorTest {

    private static final Long ANY_ID = 1L;
    private static final String VALID_NAME = "Pudding";
    private static final String EMPTY_NAME = "";
    private static final String INVALID_NAME = "Pudding!!!!!!!!!!!!!!";
    private static final String VALID_ICON = "Pudding";
    private static final String EMPTY_ICON = "";
    private static final String INVALID_ICON = "Pudding!!!!!!!!!!!!!!";

    @InjectMocks
    private ClienteValidator validator;

    @Test
    public void shouldFailWhenIsNull() {
        // given
        ClienteEntity entity = new ClienteEntity();

        // when
        List<String> violation = validator.validateCreate(entity);

        // then
        Assert.assertFalse(violation.isEmpty());
    }

    @Test
    public void shouldFailWhenIsNotNull() {
        faildInsert(ANY_ID, VALID_NAME, VALID_ICON);
    }

    @Test
    public void shouldFailWhenIsInvalid() {
        faildInsert(ANY_ID, INVALID_NAME, INVALID_ICON);
    }

    private void faildInsert(Long anyId, String validName, String validIcon) {
        // given
        ClienteEntity entity = new ClienteEntity();
        entity.setId(anyId);
        entity.setNome(validName);

        // when
        List<String> violation = validator.validateCreate(entity);

        // then
        Assert.assertFalse(violation.isEmpty());
    }
}