package xyz.carlostelles.testeDbc.business.web;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import xyz.carlostelles.testeDbc.config.Configuration;
import xyz.carlostelles.testeDbc.persistence.ClienteEntity;
import xyz.carlostelles.testeDbc.persistence.ClienteRepository;
import xyz.carlostelles.testeDbc.web.ClienteController;
import xyz.carlostelles.testeDbc.web.domain.ClienteViewBean;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Configuration.class)
@TestPropertySource(locations="classpath:test.properties")
public class ClienteControllerIT {
    private static final String ICON = "bier";
    private static final String ICON_2 = "burguer";
    private static final String NAME = "Bebidas";
    private static final String NAME_2 = "Lanches";

    @Autowired
    private ClienteController sampleController;

    @Autowired
    private ClienteRepository repository;

    @Before
    public void setUp() {
        repository.deleteAll();
    }

    @Test
    public void shouldCreateThings() {
        // given
        ClienteViewBean viewBean = new ClienteViewBean();
        viewBean.setName(NAME);

        // when
        ClienteViewBean things = sampleController.create(viewBean);

        // then
        Assert.assertNotNull(repository.findOne(things.getId()));
    }

    @Test
    public void shouldLoadThings() {
        // given
        ClienteEntity entity = new ClienteEntity();
        entity.setNome(NAME);

        ClienteEntity entity2 = new ClienteEntity();
        entity2.setNome(NAME_2);

        entity = repository.save(entity);
        entity2 = repository.save(entity2);

        ClienteViewBean viewBean = new ClienteViewBean();
        viewBean.setId(entity.getId());
        viewBean.setName(NAME);

        ClienteViewBean viewBean2 = new ClienteViewBean();
        viewBean2.setId(entity2.getId());
        viewBean2.setName(NAME_2);

        List<ClienteViewBean> expectedList = new ArrayList<>();
        expectedList.add(viewBean);
        expectedList.add(viewBean2);

        // when
        List<ClienteViewBean> listOfThings = sampleController.get();

        // then
        Assert.assertEquals(expectedList, listOfThings);
    }
}